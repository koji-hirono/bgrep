/* public domain */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

void
match(const char *buf, size_t size, const char *key, size_t len,
	size_t offset)
{
	char *p;

	for (;;) {
		p = memmem(buf, size, key, len);
		if (p == NULL) {
			break;
		}
		printf("%zu\n", offset + p - buf);
		size -= p - buf + len;
		offset += p - buf + len;
		buf = p + len;
	}
}

void
grep(int fd, const char *key, size_t len)
{
	char buf[4096];
	size_t size;
	int s;
	int r;

	s = 0;
	size = 0;
	for (;;) {
		r = read(fd, buf + s, sizeof(buf) - s);
		if (r == -1)
			break;
		if (r == 0)
			break;
		match(buf, s + r, key, len, size);
		memmove(buf, buf + s + r - (len - 1), len - 1);
		size += s + r - (len - 1);
		s = len - 1;
	}
}

void
grep_file(const char *fname, const char *key, size_t len)
{
	int fd;

	fd = open(fname, O_RDONLY);
	if (fd == -1)
		return;
	grep(fd, key, len);
	close(fd);
}

int
main(int argc, char **argv)
{
	if (argc < 3)
		return EXIT_FAILURE;

	grep_file(argv[2], argv[1], strlen(argv[1]));

	return EXIT_SUCCESS;
}
